## jhnoll@gmail.com
ROOT:=$(shell $(HOME)/bin/findup .sgrc)
SOURCE_DIR=.
BIB=references.bib
include $(ROOT)/tools/Makefile.in

# This is used by tools/Makefile.in to create 'all:' target.
INSTALL_OBJECTS=README.txt instructions.md instructions.txt instructions.pdf instructions.html

all: $(INSTALL_OBJECTS)

README.txt: ${TOOLS.dir}/templates/lab-README.md
	$(PP) -Dcomponent=${*} $(META_DATA) $< | $(PANDOC) --standalone -t plain -o $@

README.md: ${TOOLS.dir}/templates/lab-README.md
	$(PP) $< > $@



include $(ROOT)/tools/Make.rules
clean:
	rm README.txt instructions.txt instructions.html instructions.pdf


