<!--
If you see this comment, you are reading the wrong file!
View #filename(instructions.pdf) or #(instructions.html)
per instructions in #filename(README.txt).
-->

---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...

## Project Git Repository

Most assignments this semester will be submitted via git to
Bitbucket.  As such, each group needs a Bitbucket repository that they
can use to develop and submit deliverables.  This repository needs to
be accessible to the module team as well.

## Tasks

#. Elect a member of your group to be the repository "host."  This
 should be someone reliable who you expect to finish the module!

#. Create a _private_  Bitbucket repository for your group using that member's
account.  

    #. The _workspace_ and _project_ names can be anything that you will remember.

    #. The _Repository_ name should be "a_groupXX" (or "b_groupXX") where "XX" is replaced
by your group number.  Please follow this naming convention so we know which group owns the repository. 

    #. The _Access level_ should be "Private repository."
    
    #. You should include a #filename(README) file.  Choose the "Yes, with a
     tutorial (for beginners)" option (and, _read_ the tutorial!).

    #. The _Default branch name_ should be "#master_branch" so the grading
     scripts can find your deliverabls.
     
    #. Include a #filename(.gitignore) file.


#. Invite #email(#module_email) (#module_bitbucket_id) to become a member of your repository's
 team.

    #. Select "Repository settings" from bottom of  the left menu, then "User and
     group access."

    #. Enter #email(#module_email) in the "Users" dialog.

    #. Select "Write" or "Admin" from the dropdown (it's necessary to grant write
access so we can give you feedback on your coursework by committing
files to your repository).

        ![User access](bitbucket-user-access.png){height=70%}\


    #. Click the "Add" button.

    _Caution_: Make sure your repository is accessible to
#email(#module_email). Since we will be using Bitbucket for coursework
submissions after this week, if you don't make it accessible, or don't
follow the naming convention, you won't receive credit for your
submissions.  

    Also, you will need a BitBucket Educational License to have
more than five members of your repository, so be sure your repository
owner has one.

#. Invite the other members of your group to be members of your repository's
 team in the same way as above.


#. Copy the URL for your repository from BitBucket (or, from
 #filename(.git/config)).
 
#. Paste the  URL for your repository into the Canvas assignment
 "Website URL" box.

    ![Submit URL](canvas-submit-url.png)\

#. Submit the URL for your repository by the deadline (23:59 #cw_trello_due).
 
#. Have another member of your group verify that you have submitted
 the correct URL, by copying the URL from Canvas, then cloning a *new*
 workspace using #command(git clone).

### How to find your Bitbucket URL

#. Visit your Bitbucket.org page.

#. Select your repository.

    ![Select Repository](bitbucket-home.png){width=70%}\


#. Select "Clone" in the upper right corner.

    ![Bitbucket Repository](bitbucket-repo.png){width=70%}\


#. Copy the URL _only_ (**not the "git clone" part!**).

    ![Bitbucket Clone](bitbucket-clone.png){width=70%}\
