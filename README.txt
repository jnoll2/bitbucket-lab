BitBucket URL Assignment
Dr. John Noll

To view the instructions for completing this assignment, open
instructions.pdf using Acroread, or type instruction.html from the
command line, which will open a web browser and visit the file.

You can also view instructions.txt using Notepad++, or type
more instructions.txt from the command line.
